<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Class RepairRequestOffer
 *
 * @package App
 * @property User $workshop
 * @property RepairRequest $repairRequest
 * @property integer $workshop_id
 * @property integer $repair_request_id
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|RepairRequestOffer newModelQuery()
 * @method static Builder|RepairRequestOffer newQuery()
 * @method static Builder|RepairRequestOffer query()
 * @method static Builder|RepairRequestOffer whereCreatedAt($value)
 * @method static Builder|RepairRequestOffer whereId($value)
 * @method static Builder|RepairRequestOffer whereRepairRequestId($value)
 * @method static Builder|RepairRequestOffer whereUpdatedAt($value)
 * @method static Builder|RepairRequestOffer whereWorkshopId($value)
 * @mixin Eloquent
 */
class RepairRequestOffer extends Model
{
    protected $table = 'repair_request_offer';

    /**
     * @return BelongsTo
     */
    public function repairRequest(): BelongsTo
    {
        return $this->belongsTo(RepairRequest::class, 'repair_request_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function workshop(): BelongsTo
    {
        return $this->belongsTo(User::class, 'workshop_id', 'id');
    }
}
