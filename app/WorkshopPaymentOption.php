<?php

namespace App;

use App\Service\Database\Relations\HasPivot;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * Class WorkshopPaymentOption
 *
 * @package App
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|WorkshopPaymentOption newModelQuery()
 * @method static Builder|WorkshopPaymentOption newQuery()
 * @method static Builder|WorkshopPaymentOption query()
 * @method static Builder|WorkshopPaymentOption whereCreatedAt($value)
 * @method static Builder|WorkshopPaymentOption whereId($value)
 * @method static Builder|WorkshopPaymentOption whereName($value)
 * @method static Builder|WorkshopPaymentOption whereUpdatedAt($value)
 * @mixin Eloquent
 */
class WorkshopPaymentOption extends Model implements HasPivot
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workshop_payment_option';

    /**
     * @var array
     */
    protected $hidden = ['pivot', 'created_at', 'updated_at'];

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * @return array
     */
    public function getPivotFields(): array
    {
        return [];
    }
}
