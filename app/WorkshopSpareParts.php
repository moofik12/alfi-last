<?php

namespace App;

use App\Service\Database\Relations\HasPivot;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Class WorkshopSpareParts
 *
 * @package App
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|RepairRequest[] $repairRequest
 * @property-read int|null $repair_request_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|WorkshopSpareParts newModelQuery()
 * @method static Builder|WorkshopSpareParts newQuery()
 * @method static Builder|WorkshopSpareParts query()
 * @method static Builder|WorkshopSpareParts whereCreatedAt($value)
 * @method static Builder|WorkshopSpareParts whereId($value)
 * @method static Builder|WorkshopSpareParts whereName($value)
 * @method static Builder|WorkshopSpareParts whereUpdatedAt($value)
 * @mixin Eloquent
 */
class WorkshopSpareParts extends Model implements HasPivot
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workshop_spare_parts';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'user_workshop_spare_part',
            'workshop_spare_part_id',
            'user_id'
        );
    }

    public function repairRequest(): HasMany
    {
        return $this->hasMany(RepairRequest::class);
    }

    /**
     * @return array
     */
    public function getPivotFields(): array
    {
        return [];
    }
}
