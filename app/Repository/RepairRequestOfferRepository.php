<?php


namespace App\Repository;


use App\RepairRequestOffer;
use App\Service\Database\QueryFilter\ValueFilter;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

class RepairRequestOfferRepository
{
    /**
     * @param int $repairRequestId
     * @param int $workshopId
     * @throws Exception
     */
    public function deleteByRepairRequestIdAndWorkshopId(int $repairRequestId, int $workshopId): void
    {
        RepairRequestOffer::whereWorkshopId($workshopId)
            ->whereRepairRequestId($repairRequestId)
            ->delete();
    }

    /**
     * @param int $repairRequestId
     * @param int $workshopId
     * @return RepairRequestOffer|null
     */
    public function findByRepairRequestIdAndWorkshopId(int $repairRequestId, int $workshopId): ?RepairRequestOffer
    {
        return RepairRequestOffer::whereWorkshopId($workshopId)
            ->whereRepairRequestId($repairRequestId)
            ->first();
    }

    /**
     * @param int $repairRequestId
     * @return LengthAwarePaginator
     */
    public function findByRepairRequestId(int $repairRequestId): LengthAwarePaginator
    {
        return RepairRequestOffer::with(['workshop'])
            ->where('repair_request_id', $repairRequestId)
            ->paginate();
    }

    /**
     * @param int $repairRequestId
     * @param ValueFilter $filter
     * @return LengthAwarePaginator
     */
    public function findByRepairRequestIdWithFilter(int $repairRequestId, ValueFilter $filter): LengthAwarePaginator
    {
        $builder = RepairRequestOffer::with(['workshop', 'repairRequest'])
            ->where('repair_request_id', $repairRequestId);

        $builder = $filter->apply($builder);

        if ($filter->getPerPage()) {
            return $builder->paginate($filter->getPerPage());
        }

        return $builder->paginate();
    }

    /**
     * @param int $offerId
     * @param int $workshopId
     * @return RepairRequestOffer|null
     */
    public function findByOfferIdAndWorkshopId(int $offerId, int $workshopId): ?RepairRequestOffer
    {
        return RepairRequestOffer::whereId($offerId)
            ->whereWorkshopId($workshopId)
            ->first();
    }

    /**
     * @param int $repairRequestId
     * @throws Exception
     */
    public function deleteByRepairRequestId(int $repairRequestId): void
    {
        RepairRequestOffer::whereRepairRequestId($repairRequestId)->delete();
    }

    /**
     * @param int $repairRequestId
     * @param int $exceptedOfferId
     */
    public function deleteByRepairRequestIdExcept(int $repairRequestId, int $exceptedOfferId): void
    {
        RepairRequestOffer::where('repair_request_id', $repairRequestId)
            ->where('repair_request_id', '!=', $exceptedOfferId)
            ->delete();
    }

    /**
     * @param $repairRequestId
     * @param $userId
     * @return RepairRequestOffer|null
     */
    public function findByRepairRequestIdAndUserId($repairRequestId, $userId): ?RepairRequestOffer
    {
        $callbackQuery = function (Builder $query) use ($userId) {
            $query->where('user_id', $userId);
        };

        return RepairRequestOffer::whereId($repairRequestId)
            ->whereHas('repairRequest', $callbackQuery)
            ->first();
    }
}
