<?php /** @noinspection ALL */


namespace App\Repository;


use App\Service\Database\QueryFilter\ValueFilter;
use App\User;
use App\WorkshopFeedback;
use Illuminate\Pagination\LengthAwarePaginator;

class WorkshopFeedbackRepository
{
    /**
     * @return WorkshopFeedback|null
     */
    public function findLatestFeedback(): ?WorkshopFeedback
    {
        return WorkshopFeedback::latest()->first();
    }

    /**
     * @return WorkshopFeedback|null
     */
    public function findLatestFeedbackForShop(int $workshopId): ?WorkshopFeedback
    {
        return WorkshopFeedback::where('shop_id', $workshopId)
            ->latest()
            ->first();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function findLatestFeedbackForCustomerWithFilter(int $customerId, ValueFilter $filter): LengthAwarePaginator
    {
        $builder = WorkshopFeedback::where('customer_id', $customerId);

        $builder = $filter->apply($builder);

        if ($filter->getPerPage()) {
            return $builder->paginate($filter->getPerPage());
        }

        return $builder->paginate();
    }

    /**
     * @param int $workshopId
     * @return LengthAwarePaginator
     */
    public function findFeedbackForShop(int $workshopId, int $limit): LengthAwarePaginator
    {
        return User::findOrFail($workshopId)
            ->feedbacks()
            ->with('image')
            ->orderBy('id', 'DESC')
            ->paginate($limit);
    }

    /**
     * @param int $workshopId
     * @param int $userId
     * @return int
     */
    public function countShopReviewsForUserId(int $workshopId, int $userId): int
    {
        return WorkshopFeedback::with('image')
            ->whereCustomerId($userId)
            ->whereShopId($workshopId)
            ->count();
    }

    /**
     * @param $limit
     * @return LengthAwarePaginator
     */
    public function findLatestFeedbacks($limit)
    {
        return WorkshopFeedback::latest()
            ->paginate($limit);
    }

    /**
     * @param int $shopId
     * @param ValueFilter $filter
     * @return LengthAwarePaginator
     */
    public function findLatestFeedbackForShopWithFilter(int $shopId, ValueFilter $filter)
    {
        $builder = WorkshopFeedback::where('shop_id', $customerId);

        $builder = $filter->apply($builder);

        if ($filter->getPerPage()) {
            return $builder->paginate($filter->getPerPage());
        }

        return $builder->paginate();
    }
}
