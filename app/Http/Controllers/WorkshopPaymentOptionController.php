<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdatePaymentOption;
use App\Http\Resources\WorkshopPaymentOptionCollection;
use App\Service\Database\Relations\RelationsUpdater;
use App\WorkshopPaymentOption;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Model;

class WorkshopPaymentOptionController extends Controller
{
    /**
     * @var RelationsUpdater
     */
    private $relationsUpdater;

    /**
     * WorkshopServiceController constructor.
     * @param RelationsUpdater $relationsUpdater
     */
    public function __construct(RelationsUpdater $relationsUpdater)
    {
        $this->relationsUpdater = $relationsUpdater;
    }

    /**
     * @param Guard $guard
     * @return WorkshopPaymentOptionCollection
     */
    public function paymentOptions(Guard $guard): WorkshopPaymentOptionCollection
    {
        /** @var User $user */
        $user = $guard->user();
        $activePaymentOptions = $user->workshopPaymentOptions;
        $allPaymentOptions = WorkshopPaymentOption::all();

        return new WorkshopPaymentOptionCollection($activePaymentOptions, $allPaymentOptions);
    }

    /**
     * @param UpdatePaymentOption $request
     * @param Guard $guard
     * @return WorkshopPaymentOptionCollection
     */
    public function updatePaymentOptions(UpdatePaymentOption $request, Guard $guard): WorkshopPaymentOptionCollection
    {
        /** @var User|Model $user */
        $user = $guard->user();
        $allPaymentOptions = WorkshopPaymentOption::all();
        $activePaymentOptions = $this->relationsUpdater->updateManyToMany(
            $user,
            $request,
            $allPaymentOptions,
            'workshopPaymentOptions'
        );

        return new WorkshopPaymentOptionCollection($activePaymentOptions, $allPaymentOptions);
    }
}
