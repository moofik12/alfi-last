<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateActiveServices;
use App\Http\Resources\WorkshopServiceCollection;
use App\Service\Database\Relations\RelationsUpdater;
use App\User;
use App\WorkshopService as WorkshopServiceModel;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Model;

class WorkshopServiceController extends Controller
{
    /**
     * @var RelationsUpdater
     */
    private $relationsUpdater;

    /**
     * WorkshopServiceController constructor.
     * @param RelationsUpdater $relationsUpdater
     */
    public function __construct(RelationsUpdater $relationsUpdater)
    {
        $this->relationsUpdater = $relationsUpdater;
    }

    /**
     * @param Guard $guard
     * @return WorkshopServiceCollection
     */
    public function mainServices(Guard $guard): WorkshopServiceCollection
    {
        /** @var User $user */
        $user = $guard->user();
        $workshopServices = $user->workshopServices;
        $mainServices = WorkshopServiceModel::main()->get();

        return new WorkshopServiceCollection($workshopServices, $mainServices);
    }

    /**
     * @param Guard $guard
     * @return WorkshopServiceCollection
     */
    public function additionalServices(Guard $guard): WorkshopServiceCollection
    {
        /** @var User $user */
        $user = $guard->user();
        $workshopServices = $user->workshopServices;
        $additionalServices = WorkshopServiceModel::additional()->get();

        return new WorkshopServiceCollection($workshopServices, $additionalServices);
    }

    /**
     * @param UpdateActiveServices $request
     * @param Guard $guard
     * @return WorkshopServiceCollection
     */
    public function updateMainActiveServices(UpdateActiveServices $request, Guard $guard): WorkshopServiceCollection
    {
        /** @var User $user */
        $user = $guard->user();
        $mainServices = WorkshopServiceModel::main()->get();
        $workshopServices = $this->relationsUpdater->updateManyToMany(
            $user,
            $request,
            $mainServices,
            'workshopServices'
        );

        return new WorkshopServiceCollection($workshopServices, $mainServices);
    }

    /**
     * @param UpdateActiveServices $request
     * @param Guard $guard
     * @return WorkshopServiceCollection
     */
    public function updateAdditionalActiveServices(UpdateActiveServices $request, Guard $guard): WorkshopServiceCollection
    {
        /** @var User|Model $user */
        $user = $guard->user();
        $additionalServices = WorkshopServiceModel::additional()->get();
        $workshopServices = $this->relationsUpdater->updateManyToMany(
            $user,
            $request,
            $additionalServices,
            'workshopServices'
        );

        return new WorkshopServiceCollection($workshopServices, $additionalServices);
    }
}
