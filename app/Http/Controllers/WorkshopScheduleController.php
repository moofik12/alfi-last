<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateSchedule;
use App\Http\Resources\WorkshopSchedule;
use App\Service\Database\Relations\RelationsUpdater;
use App\Service\Registration\WorkshopMaker;
use App\User;
use App\WorkshopSchedule as WorkshopScheduleModel;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Model;

class WorkshopScheduleController extends Controller
{
    /**
     * @var WorkshopMaker
     */
    private $workshopMaker;

    /**
     * WorkshopScheduleController constructor.
     * @param WorkshopMaker $workshopMaker
     */
    public function __construct(WorkshopMaker $workshopMaker)
    {
        $this->workshopMaker = $workshopMaker;
    }

    /**
     * @param Guard $guard
     * @return WorkshopSchedule
     */
    public function schedule(Guard $guard): WorkshopSchedule
    {
        /** @var User $user */
        $user = $guard->user();
        $workshopSchedule = $user->workshopSchedule;

        return new WorkshopSchedule($workshopSchedule);
    }

    /**
     * @param UpdateSchedule $request
     * @param Guard $guard
     * @param RelationsUpdater $relationsUpdater
     * @return WorkshopSchedule
     */
    public function updateSchedule(UpdateSchedule $request, Guard $guard, RelationsUpdater $relationsUpdater): WorkshopSchedule
    {
        /** @var User|Model $user */
        $user = $guard->user();

        /** @var WorkshopScheduleModel $workshopSchedule */
        $workshopSchedule = $relationsUpdater->updateHasOne(
            $user,
            $request,
            'workshopSchedule'
        );

        return new WorkshopSchedule($workshopSchedule);
    }
}
