<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateWarranty;
use App\Http\Resources\WorkshopWarranty;
use App\Service\Database\Relations\RelationsUpdater;
use App\Service\Registration\WorkshopMaker;
use App\User;
use App\WorkshopWarranty as WorkshopWarrantyModel;
use Illuminate\Contracts\Auth\Guard;

class WorkshopWarrantyController extends Controller
{
    /**
     * @var WorkshopMaker
     */
    private $workshopMaker;

    /**
     * WorkshopWarrantyController constructor.
     * @param WorkshopMaker $workshopMaker
     */
    public function __construct(WorkshopMaker $workshopMaker)
    {
        $this->workshopMaker = $workshopMaker;
    }

    /**
     * @param Guard $guard
     * @return WorkshopWarranty
     */
    public function warranty(Guard $guard): WorkshopWarranty
    {
        /** @var User|User $user */
        $user = $guard->user();
        $workshopWarranty = $user->workshopWarranty;

        return new WorkshopWarranty($workshopWarranty);
    }

    /**
     * @param UpdateWarranty $request
     * @param Guard $guard
     * @param RelationsUpdater $relationsUpdater
     * @return WorkshopWarranty
     */
    public function updateWarranty(UpdateWarranty $request, Guard $guard, RelationsUpdater $relationsUpdater): WorkshopWarranty
    {
        /** @var User|User $user */
        $user = $guard->user();

        /** @var WorkshopWarrantyModel $workshopWarranty */
        $workshopWarranty = $relationsUpdater->updateHasOne(
            $user,
            $request,
            'workshopWarranty'
        );

        return new WorkshopWarranty($workshopWarranty);
    }
}
