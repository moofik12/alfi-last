<?php

namespace App\Http\Controllers\Admin;

use App\Filter\ValueFilterFactory;
use App\Http\Controllers\Controller;
use App\Http\Resources\Transformer\RepairRequest\AddOfferMetadataToRequest;
use App\RepairRequest;
use App\RepairRequestOffer;
use App\Repository\RepairRequestOfferRepository;
use App\Repository\RepairRequestRepository;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Moofik\LaravelResourceExtenstion\Extension\ExtendableResourceCollection;
use Moofik\LaravelResourceExtenstion\Extension\RestrictableResource;

class AdminRepairRequestController extends Controller
{
    /**
     * @param Guard $guard
     * @param Request $request
     * @param RepairRequestRepository $requestRepository
     * @param RepairRequestOfferRepository $requestOfferRepository
     * @return ExtendableResourceCollection
     */
    public function requests(
        Guard $guard,
        Request $request,
        RepairRequestRepository $requestRepository,
        RepairRequestOfferRepository $requestOfferRepository
    ): ExtendableResourceCollection {
        /** @var User $user */
        $user = $guard->user();
        $filter = ValueFilterFactory::createFromRequest($request);
        $requests = $requestRepository->findAllWithFilter($filter);

        $addOfferMetadataToRequest = new AddOfferMetadataToRequest($user, $requestOfferRepository);

        return ExtendableResourceCollection::extendableCollection($requests)
            ->applyTransformer($addOfferMetadataToRequest);
    }

    /**
     * @param int $requestId
     * @param Guard $guard
     * @param RepairRequestOfferRepository $requestOfferRepository
     * @return RestrictableResource
     */
    public function request(int $requestId, Guard $guard, RepairRequestOfferRepository $requestOfferRepository): RestrictableResource
    {
        /** @var User $user */
        $user = $guard->user();
        $requests = RepairRequest::findOrFail($requestId);

        $addOfferMetadataToRequest = new AddOfferMetadataToRequest($user, $requestOfferRepository);

        return RestrictableResource::make($requests)
            ->applyTransformer($addOfferMetadataToRequest);
    }

    /**
     * @param int $requestId
     * @return JsonResponse
     */
    public function deleteRequest(int $requestId): JsonResponse
    {
        RepairRequest::destroy($requestId);

        return new JsonResponse();
    }

    /**
     * @param int $requestId
     * @param Request $request
     * @param RepairRequestOfferRepository $repairRequestOfferRepository
     * @return ExtendableResourceCollection
     */
    public function repairRequestOffers(int $requestId, Request $request, RepairRequestOfferRepository $repairRequestOfferRepository): ExtendableResourceCollection
    {
        $filter = ValueFilterFactory::createFromRequest($request);
        $offers = $repairRequestOfferRepository->findByRepairRequestIdWithFilter($requestId, $filter);

        return ExtendableResourceCollection::extendableCollection($offers);
    }

    /**
     * @param int $requestId
     * @return JsonResponse
     */
    public function deleteOffer(int $requestId): JsonResponse
    {
        RepairRequestOffer::destroy($requestId);

        return new JsonResponse();
    }
}
