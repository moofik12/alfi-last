<?php

namespace App\Http\Controllers;

use App\Filter\ValueFilterFactory;
use App\Http\Resources\Pipeline\PublicWorkshopPipeline;
use App\Http\Resources\Transformer\Shop\AddCanWriteFeedback;
use App\Http\Resources\Transformer\Shop\AddLastFeedback;
use App\Repository\RatingRepository;
use App\Repository\RepairRequestRepository;
use App\Repository\WorkshopFeedbackRepository;
use App\Repository\WorkshopRepository;
use App\Service\Monitoring\TelescopeMonitor;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Moofik\LaravelResourceExtenstion\Extension\ExtendableResourceCollection;
use Moofik\LaravelResourceExtenstion\Extension\RestrictableResource;

class WorkshopController extends Controller
{
    /**
     * @param int $workshopId
     * @param Guard $guard
     * @param WorkshopRepository $workshopRepository
     * @param RepairRequestRepository $repairRequestRepository
     * @param WorkshopFeedbackRepository $feedbackRepository
     * @param RatingRepository $ratingRepository
     * @return RestrictableResource
     */
    public function workshop(
        int $workshopId,
        Guard $guard,
        WorkshopRepository $workshopRepository,
        RepairRequestRepository $repairRequestRepository,
        WorkshopFeedbackRepository $feedbackRepository,
        RatingRepository $ratingRepository
    )
    {
        TelescopeMonitor::tagDatabaseQueries('get-workshop');

        /** @var User $user */
        $user = $guard->user();

        /** @var User $workshop */
        $workshop = User::with(User::RELATIONS)
            ->where('id', $workshopId)
            ->firstOrFail();

        $workshop = new RestrictableResource($workshop);
        $defaultWorkshopPipeline = new PublicWorkshopPipeline($workshopRepository);
        $canWriteFeedback = new AddCanWriteFeedback($user, $repairRequestRepository, $feedbackRepository);
        $addLastFeedback = new AddLastFeedback($feedbackRepository, $ratingRepository);

        return $workshop
            ->applyPipeline($defaultWorkshopPipeline)
            ->applyTransformer($canWriteFeedback)
            ->applyTransformer($addLastFeedback);
    }

    /**
     * @param Request $request
     * @param WorkshopRepository $workshopRepository
     * @return ExtendableResourceCollection
     */
    public function workshops(Request $request, WorkshopRepository $workshopRepository)
    {
        TelescopeMonitor::tagDatabaseQueries('get-workshops');

        $filter = ValueFilterFactory::createFromRequest($request);

        $builderCallback = function (Builder $query) {
            $query->where('name', '=', User::USER_ROLE_WORKSHOP);
        };

        $eagerLoadRelations = [
            User::RELATION_WORKSHOP_SETTINGS,
            User::RELATION_WORKSHOP_MAKES,
            User::RELATION_WORKSHOP_SERVICES,
            User::RELATION_WORKSHOP_SPARE_PARTS,
        ];

        $workshopQueryBuilder = User::with($eagerLoadRelations)
            ->whereHas('roles', $builderCallback);

        $builder = $filter->apply($workshopQueryBuilder);

        if ($filter->getPerPage()) {
            $workshops = $builder->paginate($filter->getPerPage());
        } else {
            $workshops = $builder->paginate();
        }

        $pipeline = new PublicWorkshopPipeline($workshopRepository);

        return ExtendableResourceCollection::extendableCollection($workshops)->applyPipeline($pipeline);
    }
}
