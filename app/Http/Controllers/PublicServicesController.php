<?php

namespace App\Http\Controllers;

use App\Http\Resources\PublicServicesCollection;
use App\WorkshopService;
use Illuminate\Http\Request;

class PublicServicesController extends Controller
{
    public function mainServices()
    {
        return new PublicServicesCollection(WorkshopService::main()->get());
    }
}
