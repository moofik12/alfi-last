<?php

namespace App\Http\Controllers;

use App\Http\Resources\PublicMakeCollection;
use App\WorkshopMake;

class PublicMakeController extends Controller
{
    public function makes()
    {
        return new PublicMakeCollection(WorkshopMake::all());
    }
}
