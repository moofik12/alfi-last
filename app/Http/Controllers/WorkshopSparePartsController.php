<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateSpareParts;
use App\Http\Resources\WorkshopPaymentOptionCollection;
use App\Http\Resources\WorkshopSparePartsCollection;
use App\Service\Database\Relations\RelationsUpdater;
use App\User;
use App\WorkshopSpareParts;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Model;

class WorkshopSparePartsController extends Controller
{
    /**
     * @var RelationsUpdater
     */
    private $relationsUpdater;

    /**
     * WorkshopServiceController constructor.
     * @param RelationsUpdater $relationsUpdater
     */
    public function __construct(RelationsUpdater $relationsUpdater)
    {
        $this->relationsUpdater = $relationsUpdater;
    }

    /**
     * @param Guard $guard
     * @return WorkshopPaymentOptionCollection
     */
    public function spareParts(Guard $guard): WorkshopPaymentOptionCollection
    {
        /** @var User $user */
        $user = $guard->user();
        $activeSpareParts = $user->workshopSpareParts;
        $allSpareParts = WorkshopSpareParts::all();

        return new WorkshopPaymentOptionCollection($activeSpareParts, $allSpareParts);
    }

    /**
     * @param UpdateSpareParts $request
     * @param Guard $guard
     * @return WorkshopSparePartsCollection
     */
    public function updateSpareParts(UpdateSpareParts $request, Guard $guard): WorkshopSparePartsCollection
    {
        /** @var User|Model $user */
        $user = $guard->user();
        $allSpareParts = WorkshopSpareParts::all();
        $activeSpareParts = $this->relationsUpdater->updateManyToMany(
            $user,
            $request,
            $allSpareParts,
            'workshopSpareParts'
        );

        return new WorkshopSparePartsCollection($activeSpareParts, $allSpareParts);
    }
}
