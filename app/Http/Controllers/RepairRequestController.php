<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIncomingRepairRequest;
use App\Http\Requests\CreateRepairRequest;
use App\Http\Requests\RepairRequestRequest;
use App\Http\Requests\WorkshopGenericRequest;
use App\Http\Resources\Pipeline\RepairRequestWorkshopPipeline;
use App\Http\Resources\RepairRequest;
use App\Http\Resources\RepairRequestCollection;
use App\RepairRequest as RepairRequestModel;
use App\Repository\RepairRequestOfferRepository;
use App\Repository\RepairRequestRepository;
use App\Service\Monitoring\TelescopeMonitor;
use App\Service\RepairRequest\RepairRequestManager;
use App\Service\RepairRequest\RepairRequestOffersManager;
use App\Service\RepairRequest\RepairRequestViewsManager;
use App\User;
use Exception;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RepairRequestController extends Controller
{
    /**
     * @var RepairRequestViewsManager
     */
    private $repairRequestViewsManager;

    /**
     * @var RepairRequestOffersManager
     */
    private $repairRequestOffersManager;

    /**
     * RepairRequestController constructor.
     * @param RepairRequestViewsManager $repairRequestViewsManager
     * @param RepairRequestOffersManager $repairRequestOffersManager
     */
    public function __construct(
        RepairRequestViewsManager $repairRequestViewsManager,
        RepairRequestOffersManager $repairRequestOffersManager
    )
    {
        $this->repairRequestViewsManager = $repairRequestViewsManager;
        $this->repairRequestOffersManager = $repairRequestOffersManager;
    }

    /**
     * @param CreateRepairRequest $request
     * @param Guard $guard
     * @param RepairRequestManager $repairRequestMaker
     * @return RepairRequest
     */
    public function createBiddingRepairRequest(
        CreateRepairRequest $request,
        Guard $guard,
        RepairRequestManager $repairRequestMaker
    ) {
        TelescopeMonitor::tagDatabaseQueries('create-repair-request');

        /** @var User $user */
        $user = $guard->user();
        $repairRequest = $repairRequestMaker->createForBidding($request);
        $repairRequestMaker->attachToUser($repairRequest, $user);

        return new RepairRequest(
            $repairRequest,
            $this->repairRequestViewsManager,
            $this->repairRequestOffersManager
        );
    }

    /**
     * @param CreateIncomingRepairRequest $request
     * @param Guard $guard
     * @param RepairRequestManager $repairRequestMaker
     * @return RepairRequest
     */
    public function createSpecificRepairRequest(
        CreateIncomingRepairRequest $request,
        Guard $guard,
        RepairRequestManager $repairRequestMaker
    ) {
        TelescopeMonitor::tagDatabaseQueries('create-incoming-repair-request');

        /** @var User $user */
        $user = $guard->user();
        $repairRequest = $repairRequestMaker->createForShop($request, $request->workshopId);
        $repairRequestMaker->attachToUser($repairRequest, $user);

        return new RepairRequest(
            $repairRequest,
            $this->repairRequestViewsManager,
            $this->repairRequestOffersManager
        );
    }

    /**
     * @param WorkshopGenericRequest $request
     * @param RepairRequestRepository $repairRequestRepository
     * @param RepairRequestOfferRepository $repairRequestOfferRepository
     * @return RepairRequestCollection
     */
    public function workshopBiddingRepairRequests(
        WorkshopGenericRequest $request,
        RepairRequestRepository $repairRequestRepository,
        RepairRequestOfferRepository $repairRequestOfferRepository
    ) {
        TelescopeMonitor::tagDatabaseQueries('available-repair-request-list');

        $repairRequests = $repairRequestRepository->findSuitableForWorkshopId($request->user->id);

        $repairRequestCollection = new RepairRequestCollection(
            $repairRequests,
            $this->repairRequestViewsManager,
            $this->repairRequestOffersManager
        );
        $repairRequestPipeline = new RepairRequestWorkshopPipeline(
            $request->user,
            $repairRequestRepository,
            $repairRequestOfferRepository
        );

        return $repairRequestCollection->applyPipeline($repairRequestPipeline);
    }

    /**
     * @param WorkshopGenericRequest $request
     * @param RepairRequestRepository $repairRequestRepository
     * @param RepairRequestOfferRepository $repairRequestOfferRepository
     * @return RepairRequestCollection
     */
    public function workshopSpecificRepairRequests(
        WorkshopGenericRequest $request,
        RepairRequestRepository $repairRequestRepository,
        RepairRequestOfferRepository $repairRequestOfferRepository
    ) {
        TelescopeMonitor::tagDatabaseQueries('incoming-repair-request-list');

        $repairRequests = $repairRequestRepository->findIncomingForWorkshopId($request->user->id);

        $repairRequestCollection = new RepairRequestCollection(
            $repairRequests,
            $this->repairRequestViewsManager,
            $this->repairRequestOffersManager
        );

        $repairRequestPipeline = new RepairRequestWorkshopPipeline(
            $request->user,
            $repairRequestRepository,
            $repairRequestOfferRepository
        );

        return $repairRequestCollection->applyPipeline($repairRequestPipeline);
    }

    /**
     * @param int $repairRequestId
     * @param Guard $guard
     * @param RepairRequestRepository $repairRequestRepository
     * @param RepairRequestOfferRepository $repairRequestOfferRepository
     * @return RepairRequest
     */
    public function increaseUniqueViews(
        int $repairRequestId,
        Guard $guard,
        RepairRequestRepository $repairRequestRepository,
        RepairRequestOfferRepository $repairRequestOfferRepository
    )
    {
        TelescopeMonitor::tagDatabaseQueries('increase-unique-views');

        /** @var RepairRequestModel|null $repairRequest */
        $repairRequest = RepairRequestModel::find($repairRequestId);

        if (!$repairRequest) {
            throw new NotFoundHttpException(sprintf('Repair request with id %d not found.', $repairRequestId));
        }

        /** @var User $user */
        $user = $guard->user();
        $this->repairRequestViewsManager->addUniqueViewer($user, $repairRequest);

        $resource = new RepairRequest(
            $repairRequest,
            $this->repairRequestViewsManager,
            $this->repairRequestOffersManager
        );
        $pipeline = new RepairRequestWorkshopPipeline($user, $repairRequestRepository, $repairRequestOfferRepository);

        return $resource->applyPipeline($pipeline);
    }

    /**
     * @param int $repairRequestId
     * @return JsonResponse
     */
    public function deleteRepairRequest(int $repairRequestId)
    {
        TelescopeMonitor::tagDatabaseQueries('delete-repair-request');

        RepairRequestModel::where('id', $repairRequestId)->delete();

        return new JsonResponse();
    }

    /**
     * @param int $id
     * @param Guard $guard
     * @param RepairRequestRepository $repairRequestRepository
     * @param RepairRequestOfferRepository $repairRequestOfferRepository
     * @return RepairRequest
     */
    public function repairRequest(
        int $id,
        Guard $guard,
        RepairRequestRepository $repairRequestRepository,
        RepairRequestOfferRepository $repairRequestOfferRepository
    ) {
        TelescopeMonitor::tagDatabaseQueries('fetch-repair-request');

        /** @var User $user */
        $user = $guard->user();

        /** @var RepairRequestModel $repairRequest */
        $repairRequest = RepairRequestModel::findOrFail($id);

        $repairRequest = new RepairRequest(
            $repairRequest,
            $this->repairRequestViewsManager,
            $this->repairRequestOffersManager
        );

        $repairRequestPipeline = new RepairRequestWorkshopPipeline(
            $user,
            $repairRequestRepository,
            $repairRequestOfferRepository
        );

        return $repairRequest->applyPipeline($repairRequestPipeline);
    }

    /**
     * @param Guard $guard
     * @param RepairRequestRepository $repairRequestRepository
     * @param RepairRequestOfferRepository $repairRequestOfferRepository
     * @return RepairRequestCollection
     */
    public function userRepairRequests(
        Guard $guard,
        RepairRequestRepository $repairRequestRepository,
        RepairRequestOfferRepository $repairRequestOfferRepository
    ) {
        TelescopeMonitor::tagDatabaseQueries('user_repair_requests');

        /** @var User $user */
        $user = $guard->user();
        $repairRequests = $repairRequestRepository->findAllForUserId($user->id);

        $repairRequestCollection = new RepairRequestCollection(
            $repairRequests,
            $this->repairRequestViewsManager,
            $this->repairRequestOffersManager
        );
        $repairRequestPipeline = new RepairRequestWorkshopPipeline($user, $repairRequestRepository, $repairRequestOfferRepository);

        return $repairRequestCollection->applyPipeline($repairRequestPipeline);
    }

    /**
     * @param RepairRequestRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function denyIncomingRequest(RepairRequestRequest $request)
    {
        /** @var RepairRequestModel|null $repairRequest */
        $repairRequest = RepairRequestModel::find($request->repairRequestId);
        $repairRequest->delete();

        return new JsonResponse();
    }

    /**
     * @param RepairRequestRequest $request
     * @return JsonResponse
     */
    public function acceptIncomingRequest(RepairRequestRequest $request)
    {
        /** @var RepairRequestModel|null $repairRequest */
        $repairRequest = RepairRequestModel::find($request->repairRequestId);
        $repairRequest->is_incoming_accepted = true;
        $repairRequest->save();

        return new JsonResponse();
    }
}
