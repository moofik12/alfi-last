<?php

namespace App\Http\Controllers;

use App\WorkshopSettings;
use Illuminate\Http\Resources\Json\JsonResource;

class PublicDictionaryController extends Controller
{
    public function cities()
    {
        return new JsonResource(WorkshopSettings::distinct()->get('city')->pluck('city'));
    }
}
