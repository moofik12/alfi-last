<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateMake;
use App\Http\Resources\WorkshopMakeCollection;
use App\Service\Database\Relations\RelationsUpdater;
use App\WorkshopMake;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Model;

class WorkshopMakeController extends Controller
{
    /**
     * @var RelationsUpdater
     */
    private $relationsUpdater;

    /**
     * WorkshopServiceController constructor.
     * @param  RelationsUpdater  $relationsUpdater
     */
    public function __construct(RelationsUpdater $relationsUpdater)
    {
        $this->relationsUpdater = $relationsUpdater;
    }

    /**
     * @param  Guard  $guard
     * @return WorkshopMakeCollection
     */
    public function makes(Guard $guard): WorkshopMakeCollection
    {
        /** @var User $user */
        $user = $guard->user();
        $activeMakes = $user->workshopMakes;
        $allMakes = WorkshopMake::all();

        return new WorkshopMakeCollection($activeMakes, $allMakes);
    }

    /**
     * @param  UpdateMake  $request
     * @param  Guard  $guard
     * @return WorkshopMakeCollection
     */
    public function updateMakes(UpdateMake $request, Guard $guard): WorkshopMakeCollection
    {
        /** @var User|Model $user */
        $user = $guard->user();
        $allMakes = WorkshopMake::all();
        $activeMakes = $this->relationsUpdater->updateManyToMany(
            $user,
            $request,
            $allMakes,
            'workshopMakes'
        );

        return new WorkshopMakeCollection($activeMakes, $allMakes);
    }
}
