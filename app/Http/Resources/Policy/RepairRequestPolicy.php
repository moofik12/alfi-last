<?php


namespace App\Http\Resources\Policy;


use App\RepairRequest;
use App\Repository\RepairRequestOfferRepository;
use App\Repository\RepairRequestRepository;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Moofik\LaravelResourceExtenstion\Policy\ResourcePolicy;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class RepairRequestPolicy extends ResourcePolicy
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var RepairRequestOfferRepository
     */
    private $repairRequestOfferRepository;

    /**
     * @var RepairRequestRepository
     */
    private $repairRequestRepository;

    /**
     * AddOfferMetadataToRequest constructor.
     * @param User $user
     * @param RepairRequestRepository $repairRequestRepository
     * @param RepairRequestOfferRepository $repairRequestOfferRepository
     */
    public function __construct(
        User $user,
        RepairRequestRepository $repairRequestRepository,
        RepairRequestOfferRepository $repairRequestOfferRepository
    )
    {
        $this->user = $user;
        $this->repairRequestOfferRepository = $repairRequestOfferRepository;
        $this->repairRequestRepository = $repairRequestRepository;
    }

    /**
     * @return array
     * @var RepairRequest $resource
     */
    public function getHiddenFields($resource): array
    {
        $offersCount = $resource->repairRequestOffers->count();

        $isOwner = $resource->ownerUser->where('id', $this->user->id);

        if ($offersCount === 0 && !$isOwner) {
            return ['name', 'email', 'phone'];
        }

        return [];
    }

    /**
     * @return array
     * @throws AccessDeniedHttpException
     * @var Model $resource
     */
    public function getVisibleFields($resource): array
    {
        if (!$this->user->hasRole(
            [
                User::USER_ROLE_WORKSHOP,
                User::USER_ROLE_USER,
                User::USER_ROLE_ADMIN
            ]
        )) {
            throw new AccessDeniedHttpException();
        }

        return [];
    }
}
