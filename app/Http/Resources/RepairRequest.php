<?php

namespace App\Http\Resources;

use App\RepairRequest as RepairRequestModel;
use App\Service\RepairRequest\RepairRequestOffersManager;
use App\Service\RepairRequest\RepairRequestViewsManager;
use App\User;
use Illuminate\Http\Request;
use Moofik\LaravelResourceExtenstion\Extension\RestrictableResource;

class RepairRequest extends RestrictableResource
{
    /**
     * @var RepairRequestViewsManager
     */
    private $repairRequestViewsManager;

    /**
     * @var RepairRequestOffersManager
     */
    private $repairRequestOffersManager;

    /**
     * RepairRequest constructor.
     * @param  RepairRequestModel  $resource
     * @param  RepairRequestViewsManager  $repairRequestViewsManager
     * @param  RepairRequestOffersManager  $repairRequestOffersManager
     */
    public function __construct(
        RepairRequestModel $resource,
        RepairRequestViewsManager $repairRequestViewsManager,
        RepairRequestOffersManager $repairRequestOffersManager
    ) {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->repairRequestViewsManager = $repairRequestViewsManager;
        $this->repairRequestOffersManager = $repairRequestOffersManager;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = parent::toArray($request);
        $result['viewed'] = $this->repairRequestViewsManager->getViewersCountThroughRelation($this->resource);
        $result['offered'] = $this->repairRequestOffersManager->getOffersCountThroughRelation($this->resource);

        unset($result['user_id']);
        unset($result['service_id']);
        unset($result['spare_parts_id']);
        unset($result['make_id']);

        $createdAtFormatted = date('Y-m-d h:i A', strtotime($result['created_at']));
        $result['created_at'] = $createdAtFormatted;

        $updatedAtFormatted = date('Y-m-d h:i A', strtotime($result['updated_at']));
        $result['updated_at'] = $updatedAtFormatted;

        if (!empty($result['workshop_id'])) {
            $workshop = User::find($result['workshop_id']);
            $publicWorkshop = (new Workshop($workshop));
            $result['workshop'] = $publicWorkshop;
            unset($result['workshop_id']);
        }

        return $result;
    }
}
