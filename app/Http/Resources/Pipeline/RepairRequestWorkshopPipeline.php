<?php


namespace App\Http\Resources\Pipeline;


use App\Http\Resources\Policy\RepairRequestPolicy;
use App\Http\Resources\Transformer\RepairRequest\AddOfferMetadataToRequest;
use App\Repository\RepairRequestOfferRepository;
use App\Repository\RepairRequestRepository;
use App\User;
use Moofik\LaravelResourceExtenstion\Pipeline\ExtensionPipeline;

class RepairRequestWorkshopPipeline extends ExtensionPipeline
{
    /**
     * RepairRequestWorkshopPipeline constructor.
     * @param User $user
     * @param RepairRequestRepository $requestRepository
     * @param RepairRequestOfferRepository $repairRequestOfferRepository
     */
    public function __construct(
        User $user,
        RepairRequestRepository $requestRepository,
        RepairRequestOfferRepository $repairRequestOfferRepository
    ) {
        $this
            ->addPolicy(new RepairRequestPolicy($user, $requestRepository, $repairRequestOfferRepository))
            ->addTransformer(new AddOfferMetadataToRequest($user, $repairRequestOfferRepository));
    }
}
