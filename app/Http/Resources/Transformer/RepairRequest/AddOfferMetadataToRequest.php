<?php


namespace App\Http\Resources\Transformer\RepairRequest;


use App\RepairRequest;
use App\Repository\RepairRequestOfferRepository;
use App\User;
use Moofik\LaravelResourceExtenstion\Transformer\ResourceTransformer;

class AddOfferMetadataToRequest extends ResourceTransformer
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var RepairRequestOfferRepository
     */
    private $repairRequestOfferRepository;

    /**
     * AddOfferMetadataToRequest constructor.
     * @param User $user
     * @param RepairRequestOfferRepository $repairRequestOfferRepository
     */
    public function __construct(User $user, RepairRequestOfferRepository $repairRequestOfferRepository)
    {
        $this->user = $user;
        $this->repairRequestOfferRepository = $repairRequestOfferRepository;
    }

    /**
     * @param RepairRequest $resource
     * @param array $data
     * @return array
     */
    public function transform($resource, array $data): array
    {
        $builder = $resource
            ->repairRequestOffers
            ->where('repair_request_id', $resource->id);

        if (!$this->user->hasRole([User::USER_ROLE_MODERATOR, User::USER_ROLE_ADMIN])) {
            $builder->where('workshop_id', $this->user->id);
        }

        $offersCount = $builder->count();

        if ($offersCount === 0) {
            $data['offer_been_made'] = false;
        } else {
            $data['offer_been_made'] = true;
        }

        return $data;
    }
}
