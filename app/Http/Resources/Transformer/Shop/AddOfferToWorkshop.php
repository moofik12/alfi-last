<?php


namespace App\Http\Resources\Transformer\Shop;


use App\Repository\RepairRequestOfferRepository;
use App\User;
use Moofik\LaravelResourceExtenstion\Transformer\ResourceTransformer;

class AddOfferToWorkshop extends ResourceTransformer
{
    /**
     * @var RepairRequestOfferRepository
     */
    private $repairRequestOfferRepository;

    /**
     * @var int
     */
    private $repairRequestId;

    /**
     * AddOfferMetadataToRequest constructor.
     * @param int $repairRequestId
     * @param RepairRequestOfferRepository $repairRequestOfferRepository
     */
    public function __construct(int $repairRequestId, RepairRequestOfferRepository $repairRequestOfferRepository)
    {
        $this->repairRequestOfferRepository = $repairRequestOfferRepository;
        $this->repairRequestId = $repairRequestId;
    }

    /**
     * @param User $resource
     * @param array $data
     * @return array
     */
    public function transform($resource, array $data): array
    {
        $offer = $this->repairRequestOfferRepository->findByRepairRequestIdAndWorkshopId(
            $this->repairRequestId,
            $resource->id
        );

        $data['offer'] = $offer;

        return $data;
    }
}
