<?php


namespace App\Http\Resources\Transformer\Shop;


use App\Repository\WorkshopRepository;
use App\User;
use Moofik\LaravelResourceExtenstion\Transformer\ResourceTransformer;

class AddWorkshopRating extends ResourceTransformer
{
    /**
     * @var WorkshopRepository
     */
    private $workshopRepository;

    /**
     * WorkshopCanBeRated constructor.
     * @param WorkshopRepository $workshopRepository
     */
    public function __construct(WorkshopRepository $workshopRepository)
    {
        $this->workshopRepository = $workshopRepository;
    }

    /**
     * @param User $resource
     * @param array $data
     * @return array
     */
    public function transform($resource, array $data): array
    {
        $rating = $resource
            ->ratings()
            ->avg('rating_score');

        $data['rating'] = (float) $rating;

        return $data;
    }
}
