<?php


namespace App\Http\Resources\Transformer\Feedback;


use App\WorkshopFeedback;
use Moofik\LaravelResourceExtenstion\Transformer\ResourceTransformer;

class AddAuthorToFeedback extends ResourceTransformer
{
    /**
     * @param WorkshopFeedback $resource
     * @param array $data
     * @return array
     */
    public function transform($resource, array $data): array
    {
        $data['author'] = $resource
            ->customer
            ->full_name;

        return $data;
    }
}
