<?php


namespace App\Http\Requests\Traits;


use App\RepairRequestOffer;
use App\Repository\RepairRequestOfferRepository;
use App\Repository\RepairRequestRepository;
use App\User;
use Illuminate\Container\Container;

trait AuthorizeRepairRequestActions
{
    use ApiResourceDigitIdentifier;

    /**
     * @var int
     */
    public $repairRequestId;

    /**
     * @var RepairRequestOffer|null
     */
    public $pendingWorkshopOffer;

    /**
     * @var bool
     */
    public $doesUserHaveGivenRepairRequest;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (($repairRequestId = $this->getModelIdentifier()) === null) {
            return false;
        }

        /** @var User $user */
        $user = $this->user();

        if (!$user) {
            return false;
        }

        /** @var RepairRequestOfferRepository $repairRequestOfferRepository */
        $repairRequestOfferRepository = Container::getInstance()->get(RepairRequestOfferRepository::class);
        $workshopOffer = $repairRequestOfferRepository->findByRepairRequestIdAndWorkshopId($repairRequestId, $user->id);
        $this->pendingWorkshopOffer = $workshopOffer;

        /** @var RepairRequestRepository $repairRequestRepository */
        $repairRequestRepository = Container::getInstance()->get(RepairRequestRepository::class);
        $hasRepairRequest = $repairRequestRepository->findUserOrWorkshopHasRepairRequest($user->id, $repairRequestId);
        $this->doesUserHaveGivenRepairRequest = $hasRepairRequest;

        if ($workshopOffer || $hasRepairRequest) {
            $this->repairRequestId = $repairRequestId;

            return true;
        }

        return false;
    }
}
