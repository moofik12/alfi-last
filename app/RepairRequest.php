<?php

namespace App;

use App\Model\CastNullAttributes;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Class RepairRequest
 *
 * @package App
 * @property integer $id
 * @property string $how_fast_time
 * @property string $drive
 * @property string $registration_number
 * @property string $city
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $description
 * @property boolean $is_incoming
 * @property boolean $is_incoming_accepted
 * @property Collection|RepairRequestViewer[] $repairRequestViewers
 * @property Collection|RepairRequestOffer[] $repairRequestOffers
 * @property Collection|User[] $ownerUser
 * @property int $user_id
 * @property int|null $workshop_id
 * @property int $make_id
 * @property int $service_id
 * @property int $spare_parts_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property bool|null $is_closed
 * @property-read User|null $applicantWorkshop
 * @property-read int|null $repair_request_offers_count
 * @property-read int|null $repair_request_viewers_count
 * @property-read WorkshopMake $workshopMake
 * @property-read WorkshopService $workshopService
 * @property-read WorkshopSpareParts $workshopSpareParts
 * @method static Builder|RepairRequest newModelQuery()
 * @method static Builder|RepairRequest newQuery()
 * @method static Builder|RepairRequest query()
 * @method static Builder|RepairRequest whereCity($value)
 * @method static Builder|RepairRequest whereCreatedAt($value)
 * @method static Builder|RepairRequest whereDescription($value)
 * @method static Builder|RepairRequest whereDrive($value)
 * @method static Builder|RepairRequest whereEmail($value)
 * @method static Builder|RepairRequest whereHowFastTime($value)
 * @method static Builder|RepairRequest whereId($value)
 * @method static Builder|RepairRequest whereIsClosed($value)
 * @method static Builder|RepairRequest whereIsIncoming($value)
 * @method static Builder|RepairRequest whereIsIncomingAccepted($value)
 * @method static Builder|RepairRequest whereMakeId($value)
 * @method static Builder|RepairRequest whereName($value)
 * @method static Builder|RepairRequest wherePhone($value)
 * @method static Builder|RepairRequest whereRegistrationNumber($value)
 * @method static Builder|RepairRequest whereServiceId($value)
 * @method static Builder|RepairRequest whereSparePartsId($value)
 * @method static Builder|RepairRequest whereUpdatedAt($value)
 * @method static Builder|RepairRequest whereUserId($value)
 * @method static Builder|RepairRequest whereWorkshopId($value)
 * @mixin Eloquent
 */
class RepairRequest extends Model
{
    use CastNullAttributes;

    protected $table = 'repair_request';

    protected $with = [
        'ownerUser',
        'applicantWorkshop',
        'workshopMake',
        'workshopService',
        'workshopSpareParts',
        'repairRequestViewers',
        'repairRequestOffers',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_closed' => 'boolean',
        'is_incoming' => 'boolean',
        'is_incoming_accepted' => 'boolean',
    ];

    public function ownerUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function applicantWorkshop(): BelongsTo
    {
        return $this->belongsTo(User::class, 'workshop_id', 'id');
    }

    public function workshopMake(): BelongsTo
    {
        return $this->belongsTo(WorkshopMake::class, 'make_id', 'id');
    }

    public function workshopService(): BelongsTo
    {
        return $this->belongsTo(WorkshopService::class, 'service_id', 'id');
    }

    public function workshopSpareParts(): BelongsTo
    {
        return $this->belongsTo(WorkshopSpareParts::class, 'spare_parts_id', 'id');
    }

    public function repairRequestViewers(): HasMany
    {
        return $this->hasMany(RepairRequestViewer::class, 'repair_request_id', 'id');
    }

    public function repairRequestOffers(): HasMany
    {
        return $this->hasMany(RepairRequestOffer::class, 'repair_request_id', 'id');
    }
}
