<?php


namespace App\Service\Database\Faker;


use App\User;
use App\WorkshopMake;
use App\WorkshopPaymentOption;
use App\WorkshopSchedule;
use App\WorkshopService;
use App\WorkshopSettings;
use App\WorkshopSpareParts;
use App\WorkshopWarranty;
use Carbon\Carbon;
use Exception;
use Faker\Generator;

class WorkshopFaker
{
    /**
     * @var Generator
     */
    private $faker;

    /**
     * WorkshopFaker constructor.
     * @param Generator $faker
     */
    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }

    /**
     * @param User $user
     * @param bool $fullOptioned
     * @return User
     * @throws Exception
     */
    public function createFakeWorkshopFromUser(User $user, bool $fullOptioned = false): User
    {
        $this->createWorkshopSettings($user);
        $this->createWorkshopSchedule($user);
        $this->createWorkshopWarranty($user);
        $this->attachMainServices($user, $fullOptioned);
        $this->attachAdditionalServices($user, $fullOptioned);
        $this->attachMakes($user, $fullOptioned);
        $this->attachPaymentOptions($user, $fullOptioned);
        $this->attachSpareParts($user, $fullOptioned);

        $user->removeRole(User::USER_ROLE_USER);
        $user->assignRole(User::USER_ROLE_WORKSHOP);

        return $user;
    }


    /**
     * @param User $workshop
     * @return WorkshopSettings
     */
    private function createWorkshopSettings(User $workshop): WorkshopSettings
    {
        $workshopSettings = new WorkshopSettings();
        $workshopSettings->workshop_address = $this->faker->address;
        $workshopSettings->workshop_name = $this->faker->company;
        $workshopSettings->city = $this->faker->city;
        $workshopSettings->zip_code = $this->faker->postcode;
        $workshopSettings->business_id = $this->faker->uuid;
        $workshopSettings->workshop_phone = $this->faker->phoneNumber;
        $workshopSettings->employee = $this->faker->randomNumber(2);
        $workshopSettings->founded = Carbon::create($this->faker->year);

        return $workshop->workshopSettings()->save($workshopSettings);
    }

    /**
     * @param User $workshop
     * @return WorkshopSchedule
     * @throws Exception
     */
    private function createWorkshopSchedule(User $workshop): WorkshopSchedule
    {
        $schedule = new WorkshopSchedule();

        if ($schedule->monday_active = (bool)random_int(0, 1)) {
            $schedule->monday_from = $this->faker->time('H:m');
            $schedule->monday_to = $this->faker->time('H:m');
        }

        if ($schedule->tuesday_active = (bool)random_int(0, 1)) {
            $schedule->tuesday_from = $this->faker->time('H:m');
            $schedule->tuesday_to = $this->faker->time('H:m');
        }

        if ($schedule->wednesday_active = (bool)random_int(0, 1)) {
            $schedule->wednesday_from = $this->faker->time('H:m');
            $schedule->wednesday_to = $this->faker->time('H:m');
        }

        if ($schedule->thursday_active = (bool)random_int(0, 1)) {
            $schedule->thursday_from = $this->faker->time('H:m');
            $schedule->thursday_to = $this->faker->time('H:m');
        }

        if ($schedule->friday_active = (bool)random_int(0, 1)) {
            $schedule->friday_from = $this->faker->time('H:m');
            $schedule->friday_to = $this->faker->time('H:m');
        }

        if ($schedule->saturday_active = (bool)random_int(0, 1)) {
            $schedule->saturday_from = $this->faker->time('H:m');
            $schedule->saturday_to = $this->faker->time('H:m');
        }

        if ($schedule->sunday_active = (bool)random_int(0, 1)) {
            $schedule->sunday_from = $this->faker->time('H:m');
            $schedule->sunday_to = $this->faker->time('H:m');
        }

        return $workshop->workshopSchedule()->save($schedule);
    }

    /**
     * @param User $workshop
     * @return WorkshopWarranty
     * @throws Exception
     */
    private function createWorkshopWarranty(User $workshop): WorkshopWarranty
    {
        $warranty = new WorkshopWarranty();
        $warranty->job_warranty_months = random_int(0, 12);
        $warranty->job_warranty_driven = random_int(500, 20000);
        $warranty->assemblies_warranty_months = random_int(0, 12);
        $warranty->assemblies_warranty_driven = random_int(500, 20000);
        $warranty->additional_info = $this->faker->realText(150);

        return $workshop->workshopWarranty()->save($warranty);
    }

    /**
     * @param User $user
     * @param bool $all
     */
    private function attachMainServices(User $user,  bool $all = false)
    {
        $services = WorkshopService::main();

        if ($all === false) {
            $services->inRandomOrder()->limit(5);
        }

        $services = $services->get();

        $user->workshopServices()->saveMany($services);
    }

    /**
     * @param User $user
     * @param bool $all
     */
    private function attachAdditionalServices(User $user, bool $all = false)
    {
        $services = WorkshopService::additional();

        if ($all === false) {
            $services->inRandomOrder()->limit(5);
        }

        $services = $services->get();

        $user->workshopServices()->saveMany($services);
    }

    /**
     * @param User $user
     * @param bool $all
     * @throws Exception
     */
    private function attachMakes(User $user, bool $all = false)
    {
        if ($all === false) {
            $makes = WorkshopMake::limit(5)->inRandomOrder()->get();
        } else {
            $makes = WorkshopMake::all();
        }

        $pivotAttributes = [];

        /** @var WorkshopService $service */
        foreach ($makes as $key => $service) {
            $pivotAttributes[$key] = [
                'to' => random_int(2018, 2020),
                'from' => random_int(1970, 2017),
            ];
        }

        $user->workshopMakes()->saveMany($makes, $pivotAttributes);
    }

    /**
     * @param User $user
     * @param bool $all
     */
    private function attachPaymentOptions(User $user, bool $all = false)
    {
        if ($all === false) {
            $paymentOptions = WorkshopPaymentOption::limit(5)->inRandomOrder()->get();
        } else {
            $paymentOptions = WorkshopPaymentOption::all();
        }

        $user->workshopPaymentOptions()->saveMany($paymentOptions);
    }

    /**
     * @param User $user
     * @param bool $all
     */
    private function attachSpareParts(User $user, bool $all = false)
    {
        if ($all === false) {
            $spareParts = WorkshopSpareParts::limit(3)->inRandomOrder()->get();
        } else {
            $spareParts = WorkshopSpareParts::all();
        }

        $user->workshopSpareParts()->saveMany($spareParts);
    }
}
