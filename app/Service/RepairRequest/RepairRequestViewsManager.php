<?php


namespace App\Service\RepairRequest;


use App\RepairRequest;
use App\RepairRequestViewer;
use App\User;

class RepairRequestViewsManager
{
    /**
     * @param User $user
     * @param RepairRequest $repairRequest
     * @return RepairRequestViewer either unique or existent viewer model
     */
    public function addUniqueViewer(User $user, RepairRequest $repairRequest): RepairRequestViewer
    {
        $repairRequestViewer = $repairRequest
            ->repairRequestViewers()
            ->where('workshop_id',  $user->id)
            ->where('repair_request_id', $repairRequest->id)
            ->first();

        if (null === $repairRequestViewer) {
            $repairRequestViewer = new RepairRequestViewer();
            $repairRequestViewer->workshop()->associate($user);
            $repairRequestViewer->repairRequest()->associate($repairRequest);
            $repairRequest->repairRequestViewers()->save($repairRequestViewer);
        }

        return $repairRequestViewer;
    }

    /**
     * @param RepairRequest $repairRequest
     * @return int
     */
    public function getViewersCountThroughRelation(RepairRequest $repairRequest): int
    {
        return $repairRequest->repairRequestViewers->count();
    }
}
