<?php


namespace App\Service\RepairRequest;


use App\RepairRequest;
use App\RepairRequestOffer;
use App\Repository\RepairRequestOfferRepository;
use App\Service\Api\Problem;
use App\Service\Api\ProblemException;
use App\User;
use Exception;

class RepairRequestOffersManager
{
    /**
     * @var RepairRequestOfferRepository
     */
    private $repairRequestOfferRepository;

    /**
     * RepairRequestOffersManager constructor.
     * @param RepairRequestOfferRepository $repairRequestOfferRepository
     */
    public function __construct(RepairRequestOfferRepository $repairRequestOfferRepository)
    {
        $this->repairRequestOfferRepository = $repairRequestOfferRepository;
    }

    /**
     * @param User $user
     * @param RepairRequest $repairRequest
     * @return RepairRequestOffer
     */
    public function addUniqueOffer(User $user, RepairRequest $repairRequest): RepairRequestOffer
    {
        $repairRequestOffer = $repairRequest
            ->repairRequestOffers()
            ->where('workshop_id', '=', $user->id)
            ->where('repair_request_id', '=', $repairRequest->id)
            ->first();

        if (null === $repairRequestOffer) {
            $repairRequestOffer = new RepairRequestOffer();
            $repairRequestOffer->workshop()->associate($user);
            $repairRequestOffer->repairRequest()->associate($repairRequest);
            $repairRequest->repairRequestOffers()->save($repairRequestOffer);
        }

        return $repairRequestOffer;
    }

    /**
     * @param RepairRequest $repairRequest
     * @return int
     */
    public function getOffersCountThroughRelation(RepairRequest $repairRequest): int
    {
        return $repairRequest->repairRequestOffers->count();
    }

    /**
     * @param int $repairRequestId
     * @param int $workshopId
     */
    public function deleteOffer(int $repairRequestId, int $workshopId)
    {
        try {
            $this
                ->repairRequestOfferRepository
                ->deleteByRepairRequestIdAndWorkshopId($repairRequestId, $workshopId);
        } catch (Exception $e) {
            $problem = new Problem(500);
            $problem->setDetail($e->getMessage());
            throw new ProblemException($problem);
        }
    }

    /**
     * Mark repair request as closed (it means either workshop has done repair job successfully or customer and shop have disagreements.
     * In a case of disagreements user can open a dispute.
     * @param int $repairRequestId
     */
    public function closeOffer(int $repairRequestId): void
    {
        try {
            /** @var RepairRequest|null $repairRequest */
            $repairRequest = RepairRequest::find($repairRequestId);

            if (null === $repairRequest) {
                $problem = new Problem(404);
                $problem->setDetail(sprintf('Repair request with id %d not found.', $repairRequestId));
                throw new ProblemException($problem);
            }

            $repairRequest->is_closed = true;
            $repairRequest->save();
        } catch (Exception $e) {
            $problem = new Problem(500);
            $problem->setDetail($e->getMessage());
            throw new ProblemException($problem);
        }
    }
}
