<?php


namespace App\Service\RepairRequest;


use App\Http\Requests\CreateRepairRequest;
use App\RepairRequest;
use App\User;
use App\WorkshopMake;
use App\WorkshopService;
use App\WorkshopSpareParts;
use Illuminate\Database\Eloquent\Model;

class RepairRequestManager
{
    /**
     * @param CreateRepairRequest $request
     * @return RepairRequest
     */
    public function createForBidding(CreateRepairRequest $request): RepairRequest
    {
        $repairRequest = new RepairRequest();
        $repairRequest->workshopMake()->associate(WorkshopMake::find($request->makeId));
        $repairRequest->workshopSpareParts()->associate(WorkshopSpareParts::find($request->sparePartId));
        $repairRequest->workshopService()->associate(WorkshopService::find($request->serviceId));
        $repairRequest->drive = $request->drive;
        $repairRequest->how_fast_time = $request->howFastTime;
        $repairRequest->registration_number = $request->registrationNumber;
        $repairRequest->city = $request->city;
        $repairRequest->name = $request->name;
        $repairRequest->phone = $request->phone;
        $repairRequest->email = $request->email;
        $repairRequest->description = $request->description;

        return $repairRequest;
    }

    /**
     * @param CreateRepairRequest $request
     * @param int $shopId
     * @return RepairRequest
     */
    public function createForShop(CreateRepairRequest $request, int $shopId)
    {
        $repairRequest = $this->createForBidding($request);
        $repairRequest->workshop_id = $shopId;
        $repairRequest->is_incoming = true;

        return $repairRequest;
    }

    /**
     * @param RepairRequest $request
     * @param User $buyer
     * @return false|RepairRequest
     */
    public function attachToUser(RepairRequest $request, User $buyer)
    {
        /** @var Model $request */
        return $buyer->requests()->save($request);
    }
}
