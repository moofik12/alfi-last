<?php


namespace App\Service\Registration;


use App\Http\Requests\RegisterWorkshop;
use App\User;
use App\WorkshopSchedule;
use App\WorkshopSettings;
use App\WorkshopWarranty;
use Illuminate\Contracts\Hashing\Hasher;

class WorkshopMaker
{
    /**
     * @var Hasher
     */
    private $hasher;

    /**
     * WorkshopMaker constructor.
     * @param  Hasher  $hasher
     */
    public function __construct(Hasher $hasher)
    {
        $this->hasher = $hasher;
    }

    /**
     * @param  RegisterWorkshop  $request
     * @return User
     */
    public function createUser(RegisterWorkshop $request): User
    {
        $workshop = new User();
        $workshop->assignRole(User::USER_ROLE_WORKSHOP);
        $workshop->is_workshop = true;
        $workshop->city = $request->city;
        $workshop->email = $request->email;
        $workshop->full_name = $request->fullName;
        $workshop->password = $this->hasher->make($request->password);
        $workshop->save();

        $this->createWorkshopSettings($workshop, $request);
        $this->createWorkshopSchedule($workshop);
        $this->createWorkshopWarranty($workshop);

        return $workshop;
    }

    /**
     * @param  User  $workshop
     * @param  RegisterWorkshop  $request
     * @return WorkshopSettings
     */
    private function createWorkshopSettings(User $workshop, RegisterWorkshop $request): WorkshopSettings
    {
        $workshopSettings = new WorkshopSettings();
        $workshopSettings->workshop_address = $request->workshopAddress;
        $workshopSettings->workshop_name = $request->workshopName;
        $workshopSettings->city = $request->city;

        return $workshop->workshopSettings()->save($workshopSettings);
    }

    /**
     * @param  User  $workshop
     * @return WorkshopSchedule
     */
    private function createWorkshopSchedule(User $workshop): WorkshopSchedule
    {
        $schedule = new WorkshopSchedule();

        return $workshop->workshopSchedule()->save($schedule);
    }

    /**
     * @param  User  $workshop
     * @return WorkshopWarranty
     */
    private function createWorkshopWarranty(User $workshop): WorkshopWarranty
    {
        $warranty = new WorkshopWarranty();

        return $workshop->workshopWarranty()->save($warranty);
    }
}
