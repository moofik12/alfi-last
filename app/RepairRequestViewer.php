<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Class RepairRequestViewer
 *
 * @package App
 * @property User $workshop
 * @property RepairRequest $repairRequest
 * @property integer $workshop_id
 * @property integer $repair_request_id
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|RepairRequestViewer newModelQuery()
 * @method static Builder|RepairRequestViewer newQuery()
 * @method static Builder|RepairRequestViewer query()
 * @method static Builder|RepairRequestViewer whereCreatedAt($value)
 * @method static Builder|RepairRequestViewer whereId($value)
 * @method static Builder|RepairRequestViewer whereRepairRequestId($value)
 * @method static Builder|RepairRequestViewer whereUpdatedAt($value)
 * @method static Builder|RepairRequestViewer whereWorkshopId($value)
 * @mixin Eloquent
 */
class RepairRequestViewer extends Model
{
    protected $table = 'repair_request_viewer';

    /**
     * @return BelongsTo
     */
    public function workshop(): BelongsTo
    {
        return $this->belongsTo(User::class, 'workshop_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function repairRequest(): BelongsTo
    {
        return $this->belongsTo(RepairRequest::class, 'repair_request_id', 'id');
    }
}
