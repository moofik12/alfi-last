<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Class WorkshopSchedule
 *
 * @package App
 * @property string $monday_active
 * @property string $monday_from
 * @property string $monday_to
 * @property string $tuesday_active
 * @property string $tuesday_from
 * @property string $tuesday_to
 * @property string $wednesday_active
 * @property string $wednesday_from
 * @property string $wednesday_to
 * @property string $thursday_from
 * @property string $thursday_active
 * @property string $thursday_to
 * @property string $friday_from
 * @property string $friday_active
 * @property string $friday_to
 * @property string $saturday_active
 * @property string $saturday_from
 * @property string $saturday_to
 * @property string $sunday_active
 * @property string $sunday_from
 * @property string $sunday_to
 * @property int $id
 * @property int $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $user
 * @method static Builder|WorkshopSchedule newModelQuery()
 * @method static Builder|WorkshopSchedule newQuery()
 * @method static Builder|WorkshopSchedule query()
 * @method static Builder|WorkshopSchedule whereCreatedAt($value)
 * @method static Builder|WorkshopSchedule whereFridayActive($value)
 * @method static Builder|WorkshopSchedule whereFridayFrom($value)
 * @method static Builder|WorkshopSchedule whereFridayTo($value)
 * @method static Builder|WorkshopSchedule whereId($value)
 * @method static Builder|WorkshopSchedule whereMondayActive($value)
 * @method static Builder|WorkshopSchedule whereMondayFrom($value)
 * @method static Builder|WorkshopSchedule whereMondayTo($value)
 * @method static Builder|WorkshopSchedule whereSaturdayActive($value)
 * @method static Builder|WorkshopSchedule whereSaturdayFrom($value)
 * @method static Builder|WorkshopSchedule whereSaturdayTo($value)
 * @method static Builder|WorkshopSchedule whereSundayActive($value)
 * @method static Builder|WorkshopSchedule whereSundayFrom($value)
 * @method static Builder|WorkshopSchedule whereSundayTo($value)
 * @method static Builder|WorkshopSchedule whereThursdayActive($value)
 * @method static Builder|WorkshopSchedule whereThursdayFrom($value)
 * @method static Builder|WorkshopSchedule whereThursdayTo($value)
 * @method static Builder|WorkshopSchedule whereTuesdayActive($value)
 * @method static Builder|WorkshopSchedule whereTuesdayFrom($value)
 * @method static Builder|WorkshopSchedule whereTuesdayTo($value)
 * @method static Builder|WorkshopSchedule whereUpdatedAt($value)
 * @method static Builder|WorkshopSchedule whereUserId($value)
 * @method static Builder|WorkshopSchedule whereWednesdayActive($value)
 * @method static Builder|WorkshopSchedule whereWednesdayFrom($value)
 * @method static Builder|WorkshopSchedule whereWednesdayTo($value)
 * @mixin Eloquent
 */
class WorkshopSchedule extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workshop_schedule';

    /**
     * @var array
     */
    protected $hidden = ['user_id', 'id', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [
        'monday_active' => 'boolean',
        'tuesday_active' => 'boolean',
        'wednesday_active' => 'boolean',
        'thursday_active' => 'boolean',
        'friday_active' => 'boolean',
        'saturday_active' => 'boolean',
        'sunday_active' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'monday_active',
        'monday_from',
        'monday_to',
        'tuesday_active',
        'tuesday_from',
        'tuesday_to',
        'wednesday_active',
        'wednesday_from',
        'wednesday_to',
        'thursday_active',
        'thursday_from',
        'thursday_to',
        'friday_active',
        'friday_from',
        'friday_to',
        'saturday_active',
        'saturday_from',
        'saturday_to',
        'sunday_active',
        'sunday_from',
        'sunday_to',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
