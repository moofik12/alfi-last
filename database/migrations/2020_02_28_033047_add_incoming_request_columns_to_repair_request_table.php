<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIncomingRequestColumnsToRepairRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repair_request', function (Blueprint $table) {
            $table->boolean('is_incoming')->nullable();
            $table->boolean('is_incoming_denied')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repair_request', function (Blueprint $table) {
            //
        });
    }
}
