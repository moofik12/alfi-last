<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserWorkshopSparePartPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_workshop_spare_part', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('workshop_spare_part_id')->unsigned()->index();
            $table->foreign('workshop_spare_part_id')->references('id')->on('workshop_spare_parts')->onDelete('cascade');
            $table->primary(['user_id', 'workshop_spare_part_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_workshop_spare_part');
    }
}
