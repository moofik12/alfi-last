<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepairRequestOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repair_request_offer', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('workshop_id')->unsigned();
            $table
                ->foreign('workshop_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->bigInteger('repair_request_id')->unsigned();
            $table
                ->foreign('repair_request_id')
                ->references('id')
                ->on('repair_request')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repair_request_offer');
    }
}
