<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddViewedOfferedRepairRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('ALTER TABLE `repair_request` CONVERT TO CHARACTER SET utf8mb4');
        Schema::table('repair_request', function (Blueprint $table) {
            $table->integer('viewed')->default(0);
            $table->integer('offered')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
