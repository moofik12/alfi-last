<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserWorkshopPaymentOptionPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_workshop_payment_option', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('workshop_payment_option_id')->unsigned()->index();
            $table->foreign('workshop_payment_option_id')->references('id')->on('workshop_payment_option')->onDelete('cascade');
            $table->primary(['user_id', 'workshop_payment_option_id'], 'user_id_payment_option_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_workshop_payment_option');
    }
}
