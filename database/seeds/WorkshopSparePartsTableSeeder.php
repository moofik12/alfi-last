<?php

use Illuminate\Database\Seeder;
use App\WorkshopSpareParts;

class WorkshopSparePartsTableSeeder extends Seeder
{
    public function run()
    {
        $sparePartsData = [
            ['name' => 'Original parts'],
            ['name' => 'Same as the originals'],
            ['name' => 'Spare parts'],
            ['name' => 'Used parts'],
            ['name' => 'Customer purchased parts'],
        ];

        foreach ($sparePartsData as $sparePartData) {
            WorkshopSpareParts::create($sparePartData);
        }
    }
}
