<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class RolesSeeder extends Seeder
{
    public function run()
    {
        $permissionRoleList = Permission::create(['name' => 'list roles']);
        $permissionRoleCreate = Permission::create(['name' => 'create role']);
        $permissionRoleEdit = Permission::create(['name' => 'edit role']);
        $permissionRoleDelete = Permission::create(['name' => 'delete role']);
        $permissionWorkshopList = Permission::create(['name' => 'list workshops']);
        $permissionWorkshopView = Permission::create(['name' => 'view workshop']);
        $permissionWorkshopCreate = Permission::create(['name' => 'create workshop']);
        $permissionWorkshopEdit = Permission::create(['name' => 'edit workshop']);
        $permissionWorkshopDelete = Permission::create(['name' => 'delete workshop']);
        $permissionRequestRepairList = Permission::create(['name' => 'list repair requests']);
        $permissionRequestRepairCreate = Permission::create(['name' => 'create repair request']);
        $permissionRequestRepairRespond = Permission::create(['name' => 'respond repair request']);
        $permissionRequestRepairEdit = Permission::create(['name' => 'edit repair request']);
        $permissionRequestRepairDelete = Permission::create(['name' => 'delete repair request']);
        $permissionImpersonateUser = Permission::create(['name' => 'impersonate user']);
        $permissionAdminUsersCreate = Permission::create(['name' => 'create admin user']);
        $permissionAdminUsersView = Permission::create(['name' => 'view admin user']);
        $permissionAdminUsersDelete = Permission::create(['name' => 'delete admin user']);
        $permissionAdminUsersEdit = Permission::create(['name' => 'edit admin user']);

        /** @var Role $roleWorkshop */
        $roleWorkshop = Role::create(['name' => 'workshop']);
        /** @var Role $roleUser */
        $roleUser = Role::create(['name' => 'user']);
        /** @var Role $roleAdmin */
        $roleAdmin = Role::create(['name' => 'admin']);
        /** @var Role $roleGuest */
        $roleGuest = Role::create(['name' => 'guest']);
        /** @var Role $roleModerator */
        $roleModerator = Role::create(['name' => 'moderator']);

        $roleWorkshop->givePermissionTo(
            $permissionRequestRepairList,
            $permissionRequestRepairRespond,
            $permissionWorkshopView,
            $permissionWorkshopCreate,
            $permissionWorkshopEdit,
            $permissionWorkshopDelete
        );

        $roleUser->givePermissionTo(
            $permissionRequestRepairCreate,
            $permissionRequestRepairEdit,
            $permissionRequestRepairDelete,
            $permissionWorkshopList,
            $permissionWorkshopView
        );

        $roleAdmin->givePermissionTo(
            $permissionRoleList,
            $permissionRoleCreate,
            $permissionRoleEdit,
            $permissionRoleDelete,
            $permissionAdminUsersCreate,
            $permissionAdminUsersDelete,
            $permissionAdminUsersEdit,
            $permissionAdminUsersView,
            $permissionWorkshopList,
            $permissionWorkshopView,
            $permissionWorkshopCreate,
            $permissionWorkshopEdit,
            $permissionWorkshopDelete,
            $permissionRequestRepairList,
            $permissionRequestRepairCreate,
            $permissionRequestRepairDelete,
            $permissionRequestRepairEdit,
            $permissionImpersonateUser
        );

        $roleModerator->givePermissionTo(
            $permissionWorkshopList,
            $permissionWorkshopView,
            $permissionWorkshopCreate,
            $permissionWorkshopEdit,
            $permissionWorkshopDelete,
            $permissionRequestRepairList,
            $permissionRequestRepairCreate,
            $permissionRequestRepairDelete,
            $permissionRequestRepairEdit
        );
    }
}
