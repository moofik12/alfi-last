<?php

use App\Rating;
use App\RepairRequest;
use App\Repository\RepairRequestRepository;
use App\User;
use App\WorkshopFeedback;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RepairRequestSeeder extends Seeder
{
    /**
     * @var RepairRequestRepository
     */
    private $repairRequestRepository;

    /**
     * RepairRequestSeeder constructor.
     * @param RepairRequestRepository $repairRequestRepository
     */
    public function __construct(RepairRequestRepository $repairRequestRepository)
    {
        $this->repairRequestRepository = $repairRequestRepository;
    }

    /**
     * @param User $workshop
     * @param RepairRequest $request
     * @throws Exception
     */
    private function offerOrNot(User $workshop, RepairRequest $request)
    {
        if ((bool)random_int(0, 1)) {
            $request
                ->repairRequestOffers()
                ->create(['workshop_id' => $workshop->id, 'repair_request_id' => $request->id]);

            $request
                ->repairRequestViewers()
                ->create(['workshop_id' => $workshop->id, 'repair_request_id' => $request->id]);
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(RepairRequest::class, 100)->create();

        /** @var User $mainWorkshop */
        $mainWorkshop = User::where('email', 'workshop@alfi.me')->first();

        $this
            ->repairRequestRepository
            ->findSuitableForWorkshopId($mainWorkshop->id)
            ->map(function (RepairRequest $request) use ($mainWorkshop) {
                $this->offerOrNot($mainWorkshop, $request);
            });

        Role::findByName(User::USER_ROLE_WORKSHOP)
            ->users()
            ->get()
            ->map(function (User $workshop) {
                return $this
                    ->repairRequestRepository
                    ->findSuitableForWorkshopId($workshop->id)
                    ->map(function (RepairRequest $request) use ($workshop) {
                        $this->offerOrNot($workshop, $request);
                    });
            });

        $requests = factory(RepairRequest::class, 2)->create();

        $mainUser = User::whereEmail('user@alfi.me')->first();
        $mainWorkshop = User::whereEmail('workshop@alfi.me')->first();

        /** @var RepairRequest $request */
        foreach ($requests as $request) {
            $request->is_incoming_accepted = null;
            $request->is_incoming = true;
            $request->is_closed = false;
            $request->ownerUser()->associate($mainUser);
            $request->applicantWorkshop()->associate($mainWorkshop);
            $request->save();
        }

        $requests = factory(RepairRequest::class, 2)->create();

        /** @var RepairRequest $request */
        foreach ($requests as $request) {
            $request->is_closed = true;
            $request->ownerUser()->associate($mainUser);
            $request->applicantWorkshop()->associate($mainWorkshop);
            $request->save();

            $rating = new Rating();
            $rating->customer()->associate($mainUser);
            $rating->shop()->associate($mainWorkshop);
            $rating->repair_request_id = $request->id;
            $rating->rating_score = 5;
            $rating->save();
        }

        $feedback = new WorkshopFeedback();
        $feedback->customer()->associate($mainUser);
        $feedback->shop()->associate($mainWorkshop);
        $feedback->text = app(\Faker\Generator::class)->realText(200);
        $feedback->save();
    }
}
