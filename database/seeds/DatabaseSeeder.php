<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(WorkshopServiceSeeder::class);
        $this->call(WorkshopMakeTableSeeder::class);
        $this->call(WorkshopPaymentOptionsTableSeeder::class);
        $this->call(WorkshopSparePartsTableSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(RepairRequestSeeder::class);
    }
}
